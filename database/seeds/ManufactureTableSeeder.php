<?php

use Illuminate\Database\Seeder;

class ManufactureTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('vehicle_manufacture')->insert([
            ['id' => 1, 'name' => 'Volkswagen'],
            ['id' => 2, 'name' => 'BMW'],
            ['id' => 3, 'name' => 'Mercedes Benz'],
            ['id' => 4, 'name' => 'Audi'],
            ['id' => 5, 'name' => 'Toyota'],
            ['id' => 6, 'name' => 'Other'],
        ]);
    }
}
