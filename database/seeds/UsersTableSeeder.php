<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'id'                => 1,
            'first_name'        => 'Themba',
            'last_name'         => 'Malungani',
            'email'             => 'themba.clarence@gmail.com',
            'contact_number'    => '0813485954',
            'password'          => Hash::make('123456')
        ]);
    }
}
