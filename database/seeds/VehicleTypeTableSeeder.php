<?php

use Illuminate\Database\Seeder;

class VehicleTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('vehicle_type')->insert([
            ['id'=> 1, 'manufacture_id' => 1, 'name' => 'Polo Vivo'],
            ['id'=> 2, 'manufacture_id' => 1, 'name' => 'Golf GTI'],
            ['id'=> 3, 'manufacture_id' => 2, 'name' => '135i'],
            ['id'=> 4, 'manufacture_id' => 2, 'name' => 'M4'],
            ['id'=> 5, 'manufacture_id' => 3, 'name' => 'A45 AMG'],
            ['id'=> 6, 'manufacture_id' => 3, 'name' => 'C63 AMG'],
            ['id'=> 7, 'manufacture_id' => 4, 'name' => 'A3'],
            ['id'=> 8, 'manufacture_id' => 4, 'name' => 'RS3'],
            ['id'=> 9, 'manufacture_id' => 5, 'name' => 'Corolla'],
            ['id'=>10, 'manufacture_id' => 5, 'name' => 'Yaris'],
            ['id'=>11, 'manufacture_id' => 6, 'name' => 'Other'],
        ]);
    }
}
