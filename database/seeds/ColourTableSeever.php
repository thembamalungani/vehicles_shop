<?php

use Illuminate\Database\Seeder;

class ColourTableSeever extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('colour')->insert([
            ['id' => 1, 'name' => 'Black'],
            ['id' => 2, 'name' => 'White'],
            ['id' => 3, 'name' => 'Silver'],
            ['id' => 4, 'name' => 'Grey'],
            ['id' => 5, 'name' => 'Red'],
        ]);
    }
}
