<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ManufactureTableSeeder::class);
        $this->call(VehicleTypeTableSeeder::class);
        $this->call(ColourTableSeever::class);
        $this->call(UsersTableSeeder::class);
    }
}
