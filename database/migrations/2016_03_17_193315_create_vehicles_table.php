<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicle', function(Blueprint $table){

            $table->increments('id');
            $table->integer('user_id'); // This is the user show registered the vehicle.
            $table->smallInteger('manufacture_id')->unsigned();
            $table->tinyInteger('type_id')->unsigned();
            $table->tinyInteger('colour_id')->unsigned();
            $table->smallInteger('year')->nullable();
            $table->integer('mileage')->unsigned();

            // The vehicle owner's details should ideally be stored in a separate table
            $table->string('owner_first_name', 100);
            $table->string('owner_last_name', 100);
            $table->string('owner_email', 100)->nullable();

            $table->timestamps();
            $table->softDeletes();

            // Add foreign keys
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('manufacture_id')->references('id')->on('manufacture');
            $table->foreign('type_id')->references('id')->on('vehicle_type');
            $table->foreign('colour_id')->references('id')->on('colour');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('vehicle');
    }
}
