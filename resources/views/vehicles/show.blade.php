@extends('layouts.app')

@section('content')
    <div class="container">
        <input type="hidden" id="vehicle-id" value="{{ $vehicle->id }}">
        <div class="row">
            <h2>
                {{ $vehicle->manufacture->name }} - {{ $vehicle->type->name }}
            </h2>
            <p>
                <span><span class="bold description title">Vehicle Owner:</span> {{ $vehicle->owner_first_name  }} {{ $vehicle->owner_last_name }}</span><br/>
                <span><span class="bold description title">Owner Email Address:</span> {{ $vehicle->owner_email  }}</span><br/>
                <span><span class="bold description title">Manufacture:</span> {{ $vehicle->manufacture->name  }}</span><br/>
                <span><span class="bold description title">Type:</span> {{ $vehicle->type->name  }}</span><br/>
                <span><span class="bold description title">Year:</span> {{ $vehicle->year  }}</span><br/>
                <span><span class="bold description title">Mileage:</span> {{ $vehicle->mileage  }}</span><br/>
                <span><span class="bold description title">Date Registered:</span> {{ $vehicle->created_at->toFormattedDateString()  }}</span>
            </p>
            <button id="print-vehicle" type="button" class="btn btn-info no-print">Print</button>
            <button id="edit-vehicle" type="button" class="btn btn-primary no-print">Edit</button>
            <button id="delete-vehicle" type="button" class="btn btn-danger no-print">Delete</button>
        </div>

    </div>
@endsection