@extends('layouts.app')

@section('content')

    @if(Session::has('flash_message'))
        @include('alert', array('message' => Session::get('flash_message')))
    @endif
    <div class="container">
        <div class="row">
           <div class="row">
               <div class="alert alert-info" role="alert">Update details and click "Update" to save changes.</div>
           </div>
        </div>
    </div>
    @include('vehicles.partials.vehicle')
@endsection