@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="row">
                <div class="alert alert-info" role="alert">
                    <h4>Welcome <strong>{{ $user->first_name }} {{ $user->last_name }}</strong></h4>
                    <p>You are logged in, sweet!! You can manage you vehicles below.</p>
                    <br/>
                    <h5><strong>Summary</strong></h5>
                    <p>Number of vehicles  <span class="badge" style="margin-left: 5px">{{ $totalVehicles }}</span></p>
                </div>
            </div>
        </div>
        <div class="row">

            <div class="row">
                <button type="button" class="btn btn-success" id="add-vehicle">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                    Add New Vehicle
                </button>
            </div>
            <div class="row">
                @include('vehicles.partials.vehicle_list')
            </div>

        </div>
    </div>
@endsection