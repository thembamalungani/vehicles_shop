<div class="container">
    <div class="row">
        {!! Form::model($vehicle, array('route' => $route, 'method' => $method)) !!}
        <h3>Owner details</h3>
        <input name="user_id" type="hidden" value="{{$user->id or ''}}">
        <div class="form-group">
            {{ Form::label('owner_first_name', 'First name')}}
            {{ Form::text('owner_first_name', (old('owner_first_name') ? old('owner_first_name') : $vehicle->owner_first_name), array('class' => 'form-control')) }}
            @if ($errors->has('owner_first_name'))
                @include('field_error_message', array('message' => $errors->first('owner_first_name')))
            @endif
        </div>
        <div class="form-group">
            {{ Form::label('owner_last_name', 'Last name')}}
            {{ Form::text('owner_last_name', (old('owner_last_name') ? old('owner_last_name') : $vehicle->owner_last_name), array('class' => 'form-control')) }}
            @if ($errors->has('owner_last_name'))
                @include('field_error_message', array('message' => $errors->first('owner_last_name')))
            @endif
        </div>
        <div class="form-group">
            {{ Form::label('owner_email', 'Email address')}}
            {{ Form::text('owner_email', (old('owner_email') ? old('owner_last_name') : $vehicle->owner_email), array('class' => 'form-control')) }}
            @if ($errors->has('owner_email'))
                @include('field_error_message', array('message' => $errors->first('owner_email')))
            @endif
        </div>

        <h3>Vehicle Details</h3>
        <div class="form-group">
            {{ Form::label('manufacture_id', 'Manufacture')}}
            {{ Form::select('manufacture_id', $manufactures, (old('manufacture_id') ? :  ($vehicle->manufacture ? $vehicle->manufacture->id : 0)), array('required' => 'required', 'class' => 'form-control')) }}
            @if ($errors->has('manufacture_id'))
                @include('field_error_message', array('message' => $errors->first('manufacture_id')))
            @endif
        </div>
        <div class="form-group">
            {{ Form::label('type_id', 'Type')}}
            {{ Form::select('type_id', $types, (old('type_id') ? old('type_id') : ( $vehicle->type ? $vehicle->type->id : 0 )), array('required' => 'required','class' => 'form-control')) }}
            @if ($errors->has('type_id'))
                @include('field_error_message', array('message' => $errors->first('type_id')))
            @endif
        </div>

        <div class="form-group">
            {{ Form::label('year', 'Year')}}
            <select class="form-control" name="year" required="required">
                <option value="0" selected="selected">Select year</option>
                <!-- Looking at using Form::selectRange -->
                @foreach(range(date('Y'), 1900, -1) as $year)
                    <option {{ $year == $vehicle->year ? "selected='selected" : ''}} value="{{ $year }}">{{ $year }}</option>
                @endforeach
            </select>
            @if ($errors->has('year'))
                @include('field_error_message', array('message' => $errors->first('year')))
            @endif
        </div>
        <div class="form-group">
            {{ Form::label('colour_id', 'Colour')}}
            {{ Form::select('colour_id', $colours, (old('colour_id') ? old('colour_id') : ($vehicle->colour ? $vehicle->colour->id : 0)), array('class' => 'form-control')) }}
            @if ($errors->has('colour_id'))
                @include('field_error_message', array('message' => $errors->first('colour_id')))
            @endif
        </div>
        <div class="form-group">
            {{ Form::label('mileage', 'Mileage')}}
            {{ Form::number('mileage', ( old('mileage') ? old('mileage') : $vehicle->mileage), array('class' => 'form-control')) }}
            @if ($errors->has('mileage'))
                @include('field_error_message', array('message' => $errors->first('mileage')))
            @endif
        </div>

        <input class="btn btn-success" type="submit" value="{{ $submitButtonText  }}">
        {!! Form::close() !!}
    </div>
</div>