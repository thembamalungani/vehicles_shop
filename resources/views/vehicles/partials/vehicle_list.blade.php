@if($vehicles->count() > 0)
    <table class="table table-hover">
        <thead>
            <td></td>
            <td>#</td>
            <td>Vehicle Owner</td>
            <td>Date Registered</td>
            <td>Type/Model</td>
            <td>Manufacture</td>
            <td>Year</td>
            <td>Mileage</td>
            <td>Action</td>
        </thead>
        <tbody>
        @foreach($vehicles as $vehicle)
            <tr class="vehicle-details-row" data-vehicle-id="{{ $vehicle->id }}">
                <td class="not-clickable"><input type="checkbox"></td>
                <td>{{ ++$number }}</td>
                <td>{{ $vehicle->owner_first_name  }} {{ $vehicle->owner_last_name  }}</td>
                <td>{{ $vehicle->created_at->toFormattedDateString()  }}</td>
                <td>{{ $vehicle->type->name  }}</td>
                <td>{{ $vehicle->manufacture->name  }}</td>
                <td>{{ $vehicle->year  }}</td>
                <td>{{ $vehicle->mileage  }}</td>
                <td class="not-clickable">
                    <span class="glyphicon glyphicon-edit alert-info edit-vehicle" aria-hidden="true" data-vehicle-id="{{ $vehicle->id }}"></span>
                    <span class="glyphicon glyphicon-trash text-danger delete-vehicle" aria-hidden="true" data-vehicle-id="{{ $vehicle->id }}" style="margin-left: 8px"></span>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {{ $vehicles->render() }}
@else
    <br/>
    <div class="alert alert-warning" role="alert">You have not registered vehicles before. Register a new vehicle by cliking the button above.</div>
@endif
