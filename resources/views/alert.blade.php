<div class="container">
    <div class="row">
        <div class="alert alert-success" role="alert">
            <strong>Success</strong> {{ $message }}
        </div>
    </div>
</div>