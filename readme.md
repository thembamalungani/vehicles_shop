# Vehicles Shop Project

Vehicles Shop Project by Themba Malungani

## Setup steps:
1. Clone the repository to your local machine - `git clone git@bitbucket.org:thembamalungani/vehicles_shop.git`
2. Change directory into the repository - `cd ./vehicles_shop/`
3. Run composer install - `composer install`
4. Run phing build tool - `./vendor/bin/phing`
   This will perform below tasks 5 to 8 automatically. You can also run them manually in the oder below: 
   1. copy .env.example into .env - `cp .env.example .env`
   2. Create a sqlite db - `touch database/database.sqlite`
   3. Run migration with seeding - `php artisan migrate --seed`
   4. Generate project key - `php artisan key:generate`

5. The project is now ready, to test run - `php artisan serve` - run a local development serve. This by default will make the web application accessible at __http://localhost:8000__.
## License

## REST API ##
* Uses basic auth so you can use the built in username: themba.clarence password: 123456.
### End Points ###
* `GET /api/vehicles` options: `output=json`, `output=xml` e.g. `GET /api/vehicles?output=xml`
* CREATE a vehicle
*   `POST /api/vehicles`. Below are the required fields.
* `owner_first_name` - String value e.g. Themba
* `owner_last_name` - String value e.g. Malungani
* `owner_email` - String e.g. themba.clarence@gmail.com
* `manufacture_id` - integer e.g. 2
* `type_id` - integer e.g. 3
* `year` - integer e.g. 2016
* `colour_id` - integer e.g. 2
* `mileage` - integer e.g. 10000
* `user_id` - integer e.g. 1

* Update a vehicle
`PUT /api/vehicles/{id}` options can be any of those used when creating a vehicles with `user_id` required.

* Delete a vehicle
`DELETE /vehicles/{id}`


To be updated.