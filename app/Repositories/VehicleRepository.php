<?php

namespace App\Repositories;

use App\Vehicle;

/**
 * Class VehicleRepository
 * @package App\Repositories
 */
class VehicleRepository extends AbstractEloquentRepository
{
    /**
     * VehicleRepository constructor.
     * @param Vehicle $model
     */
    public function __construct(Vehicle $model)
    {
        parent::__construct($model);
    }

    public function all()
    {
        return $this->model->ownedByUser()->get();
    }

    public function paginate($perPage)
    {
        return $this->model->ownedByUser()->paginate($perPage); // TODO: Change the autogenerated stub
    }

}