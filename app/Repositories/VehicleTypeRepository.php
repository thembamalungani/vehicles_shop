<?php

namespace App\Repositories;


use App\VehicleType;

class VehicleTypeRepository extends AbstractEloquentRepository
{
    public function __construct(VehicleType $model)
    {
        parent::__construct($model);
    }
}