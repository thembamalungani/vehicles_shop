<?php

namespace App\Repositories;


use App\Manufacture;

class ManufactureRepository extends AbstractEloquentRepository
{
    /**
     * ManufactureRepository constructor.
     * @param Manufacture $model
     */
    public function __construct(Manufacture $model)
    {
        parent::__construct($model);
    }
}