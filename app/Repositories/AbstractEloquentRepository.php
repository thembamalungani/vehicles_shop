<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;

/**
 * Class AbstractEloquentRepository
 * @package App\Repositories
 */
class AbstractEloquentRepository
{
    /**
     * @var Model
     */
    protected $model;

    /**
     * AbstractEloquentRepository constructor.
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     * Get all resources
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function all()
    {
        return $this->model->all();
    }

    /**
     * Find a specific resource
     *
     * @param $id
     * @return mixed
     */
    public function find($id)
    {
        return $this->model->find($id);
    }

    /**
     * Create a new resource
     *
     * @param $attributes
     * @return static
     */
    public function create($attributes)
    {
        return $this->model->create($attributes);
    }

    /**
     * Delete a resource
     *
     * @param $id
     * @return int
     */
    public function destroy($id)
    {
        return $this->model->destroy($id);
    }

    /**
     * Update a resource
     *
     * @param $id
     * @param $attributes
     * @return mixed
     */
    public function update($id, $attributes)
    {
        return $this->model->find($id)->update($attributes);
    }

    /**
     * Paginate vehicles
     *
     * @param $perPage
     * @return mixed
     */
    public function paginate($perPage)
    {
        return $this->model->paginate($perPage);
    }

    /**
     * @param $keys
     * @param $coloumn
     * @return mixed
     */
    public function lists($keys, $coloumn)
    {
        return $this->model->lists($keys, $coloumn);
    }
}