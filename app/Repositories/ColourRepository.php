<?php

namespace App\Repositories;

use App\Colour;

class ColourRepository extends AbstractEloquentRepository
{

    /**
     * ColourRepository constructor.
     * @param Colour $model
     */
    public function __construct(Colour $model)
    {
        parent::__construct($model);
    }
}