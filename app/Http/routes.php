<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web', 'auth']], function () {
   // Redirect the root page to /home
   Route::get('/', function () {
        return Redirect::to('/vehicles', 301)->withInput();
   });

    Route::resource('/vehicles', 'VehicleController');
});

Route::group(['middleware' => ['web']], function () {
    Route::auth();
});


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Below are the routes that will be accessible as REST API endpoints.
|
*/
Route::group(['prefix'=>'/api', 'middleware' => ['format', 'auth.basic']], function(){
    // Vehicle resource route to handle vehicle interactions.
    Route::resource('/vehicles', 'VehicleAPIController',['except' => ['create', 'edit']]);
});
