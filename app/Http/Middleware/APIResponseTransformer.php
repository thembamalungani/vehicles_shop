<?php

namespace App\Http\Middleware;

use App\Services\ResponseTransformer;
use Closure;

class APIResponseTransformer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        switch($request->get('output')){
            case 'json': $format = 'application/json';break;
            case 'xml':  $format = 'application/xml';break;
            default:     $format = 'application/json';break;
        }

        $response = $next($request);
        $content  = (new ResponseTransformer($format, $response->getContent()))->transform();

        $response->header('Content-Type', $format);
        return response($content, $response->getStatusCode(), $response->headers->all());
    }
}
