<?php

namespace App\Http\Controllers;

use App\Services\Validation\VehicleValidator;
use App\Vehicle;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Repositories\VehicleRepository;
use Illuminate\Support\Facades\Log;

class VehicleAPIController extends Controller
{
    /**
     * @var VehicleRepository
     */
    protected $vehicleRepository;

    /**
     * @var VehicleValidator
     */
    protected $vehicleValidator;

    /**
     * VehicleController constructor.
     * @param VehicleRepository $vehicleRepository
     */
    public function __construct(VehicleRepository $vehicleRepository, VehicleValidator $vehicleValidator)
    {
        $this->vehicleRepository = $vehicleRepository;
        $this->vehicleValidator = $vehicleValidator;
    }

    /**
     * Get all vehicles
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function index()
    {
        // This action is not praticall with current API requirements as user should only see ther resource.
        return response($this->vehicleRepository->all(), 200);
    }

    /**
     * View a vehicle resource
     *
     * @param Vehicle $vehicle
     * @return Vehicle|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function show(Vehicle $vehicle)
    {
        if ($vehicle instanceof $vehicle){
            return response($vehicle, 200);
        }

        return response(null, 404);
    }

    /**
     * Save a new vehicle resource
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function store(Request $request)
    {
        if ($this->vehicleValidator->validate($request->except('output'))->fails()){
            return response($this->vehicleValidator->messages(), 400);
        }

        $vehicle = $this->vehicleRepository->create($request->except('output'));

        if ($vehicle instanceof Vehicle){

            return response($vehicle->toJson(), 201,[
                'Location'      => route('api.vehicles.show', $vehicle->id)
            ]);

        }
        return response(null, 500);
    }

    /**
     * Update the vehicle resource
     *
     * @param Request $request
     * @return bool|int
     */
    public function update(Request $request, Vehicle $vehicle)
    {
        if ($this->vehicleValidator->validate($request->except('output'))->fails()){
            return response($this->vehicleValidator->messages(), 400);
        }

        $isUpdated = $this->vehicleRepository->update($vehicle->id, $request->except('output'));
        if ($isUpdated){
            return response($request->except('output'), 200);
        }

        // Updating the resource failed, send back serve error.
        return response(null, 500);
    }

    /**
     * Delete resource
     *
     * @param Vehicle $vehicle
     * @return int
     */
    public function destroy(Vehicle $vehicle)
    {
        if ($this->vehicleRepository->destroy($vehicle->id)){
            return response(null, 200);
        }

        return response(null, 500);
    }

}
