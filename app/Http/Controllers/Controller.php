<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    // user property which will store the authenticated user.
    protected $user;
  
    public function __construct()
    {
      // Get authenticated user and share it will all views.
      $this->user = \Auth::user();
      view()->share('user', $this->user);
    }
}
