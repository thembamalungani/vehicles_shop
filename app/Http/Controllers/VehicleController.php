<?php

namespace App\Http\Controllers;

use App\Repositories\ColourRepository;
use App\Repositories\ManufactureRepository;
use App\Repositories\VehicleRepository;
use App\Repositories\VehicleTypeRepository;
use App\Vehicle;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Session;

/**
 * Class VehicleController
 * @package App\Http\Controllers
 */
class VehicleController extends Controller
{
    /**
     * @var Auth::user
     */
    protected $user;

    /**
     * @var VehicleRepository
     */
    protected $vehicleRepository;

    /**
     * @var ManufactureRepository
     */
    protected $manufcatureRepository;

    /**
     * @var VehicleTypeRepository
     */
    protected $vehicleTypeRepository;

    /**
     * @var ColourRepository
     */
    protected $colourRepository;

    /**
     * VehicleController constructor.
     * @param VehicleRepository $vehicleRepository
     */
    public function __construct(
        VehicleRepository $vehicleRepository,
        ManufactureRepository $manufactureRepository,
        VehicleTypeRepository $vehicleTypeRepository,
        ColourRepository $colourRepository)
    {
        parent::__construct();

        $this->user                  = auth()->user();
        $this->vehicleRepository     = $vehicleRepository;
        $this->manufcatureRepository = $manufactureRepository;
        $this->vehicleTypeRepository = $vehicleTypeRepository;
        $this->colourRepository      = $colourRepository;
    }

    /**
     * Show list of vehicles
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $totalVehicles = count($this->vehicleRepository->all());
        $vehicles      = $this->vehicleRepository->paginate(10);
        $number        = 0;

        return view('vehicles.index', compact('vehicles', 'totalVehicles', 'number'));
    }

    /**
     * Show vehicle details page
     *
     * @param Vehicle $vehicle
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Vehicle $vehicle)
    {
        return view('vehicles.show', array('vehicle' => $vehicle));
    }

    /**
     * Show form to edit vehicle details
     *
     * @param Vehicle $vehicle
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Vehicle $vehicle)
    {
        $route            = array('vehicles.update', $vehicle->id);
        $method           = 'PUT';
        $submitButtonText = 'Update';
        $manufactures     = array_merge([ 0 => 'Select manufacture'], $this->manufcatureRepository->lists('name', 'id')->all());
        $types            = array_merge([ 0 => 'Select type'],$this->vehicleTypeRepository->lists('name', 'id')->all());
        $colours          = array_merge([ 0 => 'Select colour'],$this->colourRepository->lists('name', 'id')->all());

        return view('vehicles.edit', compact('route', 'method', 'submitButtonText', 'vehicle', 'manufactures', 'types', 'colours'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Requests\StoreVehicleRequest $request, Vehicle $vehicle)
    {
        if ($this->vehicleRepository->update($vehicle->id, $request->except('_token'))){
            Session::flash('flash_message','Vehicle details successfully updated');
            return redirect()->back();
        }

        Session::flash('error','Error while processing');
        return redirect()->back();
    }

    /**
     * Show the form to create a new vehicle
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Request $request)
    {
        $route            = 'vehicles.store';
        $method           = 'POST';
        $submitButtonText = 'Save';
        $vehicle          = new Vehicle;
        $manufactures     = array_merge([ 0 => 'Select manufacture'], $this->manufcatureRepository->lists('name', 'id')->all());
        $types            = array_merge([ 0 => 'Select type'],$this->vehicleTypeRepository->lists('name', 'id')->all());
        $colours          = array_merge([ 0 => 'Select colour'],$this->colourRepository->lists('name', 'id')->all());

        return view('vehicles.create', compact('route', 'method', 'submitButtonText', 'vehicle', 'manufactures', 'types', 'colours'));
    }

    /**
     * Save new vehicle
     *
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function store(Requests\StoreVehicleRequest $request)
    {
        if ($this->vehicleRepository->create($request->except('_token'))){
            Session::flash('flash_message','Vehicle created successfully.');
            return redirect()->route('vehicles.index');
        }

        Session::flash('error','Error while creating vehicle.');
        return redirect()->back();
    }

    /**
     * Delete a vehicle
     *
     * @param Vehicle $vehicle
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Vehicle $vehicle)
    {
        if ($this->vehicleRepository->destroy($vehicle->id)){
            Session::flash('flash_message','Vehicle successfully deleted.');
        }

        Session::flash('error','Error while creating vehicle.');
        return redirect()->back();
    }
}
