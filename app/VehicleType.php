<?php
/**
 * Created by PhpStorm.
 * User: thembamalungani
 * Date: 2016/03/17
 * Time: 10:41 PM
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class VehicleType extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'vehicle_type';

    protected $fillable = [
        'name'
    ];

    /**
     * Get vehicle type manufacture
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function manufacture()
    {
        return $this->belongsTo('App\Manufacture', 'manufacture_id');
    }
}