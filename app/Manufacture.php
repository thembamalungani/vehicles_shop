<?php
/**
 * Created by PhpStorm.
 * User: thembamalungani
 * Date: 2016/03/17
 * Time: 10:39 PM
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class Manufacture extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'vehicle_manufacture';

    public $fillable = [
      'name'
    ];

    public function types()
    {
        return $this->hasMany(VehicleType::class);
    }
}