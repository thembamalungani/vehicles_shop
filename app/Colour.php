<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Colour extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'colour';

    public $fillable = [
        'name'
    ];
}