<?php
namespace App\Services;

use Spatie\ArrayToXml\ArrayToXml;

/**
 * Class ResponseTransformer
 * @package App\Services
 */
class ResponseTransformer
{
    protected $outputFormat;
    protected $data;

    /**
     * ResponseTransformer constructor.
     *
     * @param string $outputFormat
     * @param $data
     */
    public function __construct($outputFormat, $data)
    {
        $this->outputFormat = $outputFormat;
        $this->data         = $data;
    }

    /**
     * Transform data into required format
     */
    public function transform()
    {
        // this will probably come from a config file
        $supported = array('application/xml', 'application/json');

        // If format not supported
        if (!in_array($this->outputFormat, $supported)){$this->outputFormat = 'json';}
        // if data is a json string
        if (is_string($this->data)){$this->data = json_decode($this->data);}
        // if data is an object
        if (is_object($this->data)){$this->data = get_object_vars($this->data);}
        // if data could be converted to an array.
        if (!is_array($this->data)){ return null;}

        switch ($this->outputFormat) {
            case 'application/xml': $this->data  = ArrayToXml::convert($this->data);break;
            case 'application/json':$this->data  = $this->data;break;
        }

        return $this->data;

    }
}