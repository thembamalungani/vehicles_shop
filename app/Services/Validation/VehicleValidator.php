<?php

namespace App\Services\Validation;

use App\Vehicle;
use Illuminate\Support\Facades\Validator;

/**
 * Class VehicleValidator
 * @package App\Services\Validation
 */
class VehicleValidator
{
    /**
     * @var Vehicle
     */
    protected $vehicle;

    /**
     * VehicleValidator constructor.
     * @param Vehicle $vehicle
     */
    public function __construct(Vehicle $vehicle)
    {
        $this->vehicle = $vehicle;
    }

    /**
     * @var Validator
     */
    protected $validator;

    /**
     * @param $data
     * @return Validator
     */
    public function validate($data)
    {
        $this->validator = Validator::make($data, $this->vehicle->rules);
        return $this->validator;
    }

    /**
     * @return array
     */
    public function messages()
    {
        if ($this->validator){
            return $this->validator->messages();
        }

        return null;
    }
}