<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

/***
 * Class Vehicle
 * @package App
 */
class Vehicle extends Model
{
    /**
     * Enable soft deletes.
     */
    use SoftDeletes;

    protected $primaryKey = 'id';
    protected $table = 'vehicle';

    /**
     * Fields allowed in mass assignment
     * @var array
     */
    protected $fillable = [
        'user_id',
        'manufacture_id',
        'type_id',
        'colour_id',
        'year',
        'mileage',
        'owner_first_name',
        'owner_last_name',
        'owner_email'
    ];

    /**
     * Model validate rules, ideally should put this is a validation service provider.
     * @var array
     */
    protected $rules = [
        'user_id'           => 'required|numeric|not_in:0',
        'manufacture_id'    => 'required|numeric|not_in:0',
        'type_id'           => 'required|numeric|not_in:0',
        'colour_id'         => 'required|numeric|not_in:0',
        'year'              => 'numeric|not_in:0',
        'mileage'           => 'numeric|min:10',
        'owner_first_name'  => 'required|min:3',
        'owner_last_name'   => 'required|min:2',
        'owner_email'       => 'required|email'
    ];

    public function getRulesAttribute()
    {
        return $this->rules;
    }

    /**
     * Get the manufature of the vehicle
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function manufacture()
    {
        return $this->belongsTo('App\Manufacture', 'manufacture_id');
    }

    /**
     * Get the type of the vehicle
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function type()
    {
        return $this->belongsTo('App\VehicleType', 'type_id');
    }

    /**
     * Get the colour of the vehicle
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function colour()
    {
        return $this->belongsTo('App\Colour', 'colour_id');
    }

    /**
     * Scope of query to only include the vehicles owned by authenticated user.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOwnedByUser($query)
    {
        return $query->where('user_id', Auth::user()->id);
    }
}
