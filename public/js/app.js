/**
 * Created by thembamalungani on 2016/03/19.
 */

// When the DOM is ready, attach event handlers
$(document).ready(function(){
    $('.delete-vehicle').click(function(){
        var vehicleID = $(this).data('vehicle-id');
        deleteVehicle(vehicleID, '/vehicles');
    });

    /**
     * Redirect to vehicle view page when vehicle row clicked.
     */
    $('.vehicle-details-row td').click(function(){
        // If row is clickable
        if (!($(this).hasClass('not-clickable'))){
            var vehicleID = $(this).closest('tr').data('vehicle-id');

            if (vehicleID){
                window.location = '/vehicles/' + vehicleID;
            }
        }

    });

    /**
     * Redirect to vehicle edit page
     */
    $('.edit-vehicle').click(function(){
        var vehicleID = $(this).closest('tr').data('vehicle-id');
        editVehicle(vehicleID);
    });

    /**
     * Redirect to new vehicle form
     */
    $('#add-vehicle').click(function(){
        window.location = '/vehicles/create';
    });

    $('#print-vehicle').click(function(){
        window.print();
    });

    $('#delete-vehicle').click(function(){
        var vehicleID = $('#vehicle-id').val();
        deleteVehicle(vehicleID, '/vehicles');
    });

    $('#edit-vehicle').click(function(){
        var vehicleID = $('#vehicle-id').val();
        editVehicle(vehicleID);
    });

    $('.alert.alert-success').fadeOut(5000);

});

/**
 * Delete a vehicle by perfirming a json call to the vehicles API.
 *
 * @param vehicleID The vehicle to be deleted
 * @param redirectLocation The location to redirect to after deleting.
 */
function deleteVehicle(vehicleID, redirectLocation)
{
    swal(
        {
            title: 'Confirm delete!!',
            text: 'Are you sure you want to delete?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        },
        function(){
            $.ajax({
                url: '/api/vehicles/' + vehicleID,
                method: 'DELETE',
                success:setTimeout(function(){
                    swal({
                        title: 'Deleted!!',
                        text: 'The vehicle has been deleted.',
                        type: 'success'
                    }, function(){
                        window.location = redirectLocation;
                    })
                }, 2000),
                username: 'themba.clarence@gmail.com',
                password: '123456'
            });
        }
    );
}

/**
 * Redirect to vehicle edit page
 * @param vehicleID
 */
function editVehicle(vehicleID)
{
    if (vehicleID){
        window.location = '/vehicles/' + vehicleID + '/edit';
    }
}